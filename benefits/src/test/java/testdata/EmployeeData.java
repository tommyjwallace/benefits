package testdata;

public class EmployeeData {

    public String firstNameND() {
        return "Jack";
    }

    public String lastNameND() {
        return "Kirby";
    }

    public int dependents() {
        return 3;
    }

}
