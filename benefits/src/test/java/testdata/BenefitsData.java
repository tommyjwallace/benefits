package testdata;

public class BenefitsData {

    public int paycheckAmount() {
        return 2000;
    }

    public int paycheckFrequency() {
        return 26;
    }

    public int benefitsCost() {
        return 1000;
    }

    public int dependentCost() {
        return 500;
    }
}
