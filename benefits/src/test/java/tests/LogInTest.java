package tests;

import org.testng.Assert;
import org.testng.annotations.Test;

public class LogInTest extends BenefitsAbstractTest {

    @Test(enabled = true, groups = {"Smoke", "Regression"})
    public void verifyLogIn() {

        // Navigate to Log In page
        logInPage.openLogInPage();
        Assert.assertTrue(logInPage.isLogInPageDisplayed());
        print("Navigated to Log In page");

        // Log into Benefits Dashboard
        logInPage.login(logInData.getUsername(), logInData.getPassword());
        Assert.assertTrue(dashboardPage.isDashboardPageDisplayed());
        print("Logged in to Dashboard page");
    }
}