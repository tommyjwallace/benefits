package tests;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import pageobjects.DashboardPage;
import pageobjects.LogInPage;
import testdata.BenefitsData;
import testdata.EmployeeData;
import testdata.LogInData;
import util.Driver;

public class BenefitsAbstractTest {

    protected Driver driverInstance;
    protected WebDriver driver;

    // Data
    LogInData logInData;
    EmployeeData employeeData;
    BenefitsData benefitsData;

    // Page Objects
    LogInPage logInPage;
    DashboardPage dashboardPage;

    @BeforeMethod(alwaysRun = true)
    public void setup() {

        // Setup
        driverInstance = new Driver();
        driver = driverInstance.getDriver();

        // Data
        logInData = new LogInData();
        employeeData = new EmployeeData();
        benefitsData = new BenefitsData();

        // Page Objects
        logInPage = new LogInPage(driver);
        dashboardPage = new DashboardPage(driver);
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown() {
        driver.quit();
    }

    // Global Methods
    public void print(String statement) {
        System.out.println(statement);
    }
}
