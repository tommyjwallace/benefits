package tests;

import org.testng.Assert;
import org.testng.annotations.Test;

public class DashboardTest extends BenefitsAbstractTest {

    @Test(enabled = true, groups = {"Regression"})
    public void verifyAddEmployeeNoDiscount() throws InterruptedException {

        // Navigate to Log In page
        logInPage.openLogInPage();
        Assert.assertTrue(logInPage.isLogInPageDisplayed());
        print("Navigated to Log In page");

        // Log into Benefits Dashboard
        logInPage.login(logInData.getUsername(), logInData.getPassword());
        Assert.assertTrue(dashboardPage.isDashboardPageDisplayed());
        print("Logged in to Dashboard page");

        // Add employee data into system (First Name does not begin with “A” or “a”)
        dashboardPage.clickAddEmployeeButton();
        dashboardPage.addEmployeeData(employeeData.firstNameND(), employeeData.lastNameND(), employeeData.dependents());
        dashboardPage.clickSubmitButton();
        print("New employee record created");

        // Verify First Name is correct
        Assert.assertEquals(dashboardPage.getNewEmployeeFirstName(),employeeData.firstNameND());
        print("First Name is correctly displayed: " + dashboardPage.getNewEmployeeFirstName());

        // Verify Last Name is correct
        Assert.assertEquals(dashboardPage.getNewEmployeeLastName(),employeeData.lastNameND());
        print("Last Name is correctly displayed: " + dashboardPage.getNewEmployeeLastName());

        // Verify Dependents are correct
        Assert.assertEquals(dashboardPage.getNewEmployeeDependents(),employeeData.dependents());
        print("Dependents are correctly displayed: " + dashboardPage.getNewEmployeeDependents());

        // Verify Salary calculation is correct
        Assert.assertTrue(dashboardPage.isNewEmployeeSalaryCorrect(benefitsData.paycheckAmount(),
                benefitsData.paycheckFrequency()));
        print("Salary is correct: [" + benefitsData.paycheckAmount() + " * " +
                benefitsData.paycheckFrequency() + "] = " + dashboardPage.getNewEmployeeSalary());

        // Verify Gross Pay calculation is correct
        Assert.assertTrue(dashboardPage.isNewEmployeeGrossPayCorrect(benefitsData.paycheckAmount()));
        print("Gross Pay is correct: " + dashboardPage.getNewEmployeeGrossPay());

        // Verify Benefit Cost calculation is correct
        Assert.assertTrue(dashboardPage.isNewEmployeeBenefitCostCorrect(benefitsData.paycheckFrequency(),
                benefitsData.benefitsCost(), benefitsData.dependentCost(), dashboardPage.getNewEmployeeDependents()));
        print("Benefit Cost is correct: [(" + benefitsData.benefitsCost() + " + (" +
                dashboardPage.getNewEmployeeDependents() + " * " + benefitsData.dependentCost() + ")) / " +
                benefitsData.paycheckFrequency() + "] = " + dashboardPage.getNewEmployeeBenefitCost());

        // Verify Net Pay calculation is correct
        Assert.assertTrue(dashboardPage.isNewEmployeeNetPayCorrect(dashboardPage.getNewEmployeeGrossPay(),
                dashboardPage.getNewEmployeeBenefitCost()));
        print("Net Pay is correct: [" + dashboardPage.getNewEmployeeGrossPay() + " - " +
                dashboardPage.getNewEmployeeBenefitCost() + "] = " + dashboardPage.getNewEmployeeNetPay());
    }

}
