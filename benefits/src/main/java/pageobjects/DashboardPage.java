package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static util.Driver.driver;

public class DashboardPage extends BenefitsPage {

    // Web Elements
    @FindBy(id = "btnAddEmployee")
    WebElement btnAddEmployee;
    @FindBy(css = "#employees-form > div:nth-child(1) > div > input")
    WebElement txtFirstName;
    @FindBy(css = "#employees-form > div:nth-child(2) > div > input")
    WebElement txtLastName;
    @FindBy(css = "#employees-form > div:nth-child(3) > div > input")
    WebElement txtDependants;
    @FindBy(css = "#employees-form > div:nth-child(4) > div > button.btn.btn-primary")
    WebElement btnSubmit;
    @FindBy(css = "#employee-table > tbody > tr:nth-child(2) > td:nth-child(2)")
    WebElement lblFirstName;
    @FindBy(css = "#employee-table > tbody > tr:nth-child(2) > td:nth-child(3)")
    WebElement lblLastName;
    @FindBy(css = "#employee-table > tbody > tr:nth-child(2) > td:nth-child(4)")
    WebElement lblSalary;
    @FindBy(css = "#employee-table > tbody > tr:nth-child(2) > td:nth-child(5)")
    WebElement lblDependents;
    @FindBy(css = "#employee-table > tbody > tr:nth-child(2) > td:nth-child(6)")
    WebElement lblGrossPay;
    @FindBy(css = "#employee-table > tbody > tr:nth-child(2) > td:nth-child(7)")
    WebElement lblBenefitCost;
    @FindBy(css = "#employee-table > tbody > tr:nth-child(2) > td:nth-child(8)")
    WebElement lblNetPay;


    // Constructor
    public DashboardPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    // Page Methods
    public boolean isDashboardPageDisplayed() {
        if(driver.findElement(By.id("header")).isDisplayed()) {
            return true;
        }
        return false;
    }

    public void clickAddEmployeeButton() throws InterruptedException {
        btnAddEmployee.click();
        sleepTime(3000);
    }

    public void addEmployeeData(String firstName, String lastName, int dependants) {
        txtFirstName.sendKeys(firstName);
        txtLastName.sendKeys(lastName);
        txtDependants.sendKeys(String.valueOf(dependants));
    }

    public void clickSubmitButton() throws InterruptedException {
        btnSubmit.click();
        sleepTime(3000);
    }

    // Data Retrieval Methods
    public String getNewEmployeeFirstName() {
        return lblFirstName.getText();
    }

    public String getNewEmployeeLastName() {
        return lblLastName.getText();
    }

    public BigDecimal getNewEmployeeSalary() {
        String employeeSalary = lblSalary.getText();
        BigDecimal salary = new BigDecimal(employeeSalary);
        return salary;
    }

    public int getNewEmployeeDependents() {
        String employeeDependents = lblDependents.getText();
        int dependents = Integer.parseInt(employeeDependents);
        return dependents;
    }

    public BigDecimal getNewEmployeeGrossPay() {
        String employeeGrossPay = lblGrossPay.getText();
        BigDecimal grossPay = new BigDecimal(employeeGrossPay);
        return grossPay;
    }

    public BigDecimal getNewEmployeeBenefitCost() {
        String employeeBenefitCost = lblBenefitCost.getText();
        BigDecimal benefitCost = new BigDecimal(employeeBenefitCost);
        return benefitCost;
    }

    public BigDecimal getNewEmployeeNetPay() {
        String employeeNetPay = lblNetPay.getText();
        BigDecimal netPay = new BigDecimal(employeeNetPay);
        return netPay;
    }

    // Calculation Methods
    public boolean isNewEmployeeSalaryCorrect(int paycheckAmount, int paycheckFrequency) {
        BigDecimal calculatedAmount = new BigDecimal(paycheckAmount).multiply(new BigDecimal(paycheckFrequency));
        calculatedAmount = calculatedAmount.setScale(2, BigDecimal.ROUND_HALF_UP);

        if(calculatedAmount.equals(getNewEmployeeSalary())){
            return true;
        }
        return false;
    }

    public boolean isNewEmployeeGrossPayCorrect(int paycheckAmount) {
        BigDecimal grossPay = new BigDecimal(paycheckAmount);

        if(grossPay.equals(getNewEmployeeGrossPay())){
            return true;
        }
        return false;
    }

    public boolean isNewEmployeeBenefitCostCorrect(int paycheckFrequency, int benefitsCost, int dependentCost, int dependents) {
        BigDecimal totalDependentsCost = new BigDecimal(dependentCost).multiply(new BigDecimal(dependents));
        BigDecimal employeeCost = new BigDecimal(benefitsCost);
        BigDecimal totalCost = totalDependentsCost.add(employeeCost);
        totalCost = totalCost.divide(new BigDecimal(paycheckFrequency), 2, RoundingMode.HALF_UP);

        if(totalCost.equals(getNewEmployeeBenefitCost())){
            return true;
        }
        return false;
    }

    public boolean isNewEmployeeNetPayCorrect(BigDecimal grossPay, BigDecimal benefitCost) {
        BigDecimal netPay = getNewEmployeeGrossPay().subtract(getNewEmployeeBenefitCost());
        netPay = netPay.setScale(2, BigDecimal.ROUND_HALF_UP);

        if(netPay.equals(getNewEmployeeNetPay())){
            return true;
        }
        return false;
    }

}
