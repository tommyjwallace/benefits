package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.io.File;

import static util.Driver.driver;

public class LogInPage extends BenefitsPage {

    // Web Elements
    @FindBy(className = "form-username")
    WebElement txtUsername;
    @FindBy(className = "form-password")
    WebElement txtPassword;
    @FindBy(id = "btnLogin")
    WebElement btnLogin;

    // Constructor
    public LogInPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    // Page Methods
    public void openLogInPage() {
        String filePath = new File(System.getProperty("user.dir")).getParent() + "/resources/login.html";
        driver.get(filePath);
    }

    public boolean isLogInPageDisplayed() {
        if(driver.getCurrentUrl().contains("login.html")) {
            return true;
        }
        return false;
    }

    public void login(String username, String password) {
        txtUsername.sendKeys(username);
        txtPassword.sendKeys(password);
        btnLogin.click();
    }
}
